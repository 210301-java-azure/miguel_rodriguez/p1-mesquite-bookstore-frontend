<h2>Design Patterns:</h2>

<ul>
<li>Model-View-Controller [FRONTEND]</li> 
<li>Singleton [BACKEND]</li> 
</ul>

<h2>Bootstrap templates</h2>
<ul>
    Custom Components
    <ul>
        <li>Cover [Home]: A one-page template for building home pages.</li> 
        <li>Sign-in [Login. Contact Us.]: Custom form layout and design for a simple sign in form.</li>
        <li>Offcanvas navbar [Website]: Reasponsive and expandable navbar.</li>
    </ul>
    Integrations
    <ul>
        <li>Masonry [About Us]: Bootsrap grid and the Masonry layout.</li> 
    </ul>
</ul>

