document.getElementById("addingFeedback-form").addEventListener("submit", addFeedback);

function addFeedback(event) {
    event.preventDefault();
    const firstNameOfFeedback = document.getElementById("inputFirstName").value;
    const lastNameOfFeedback = document.getElementById("inputLastName").value;
    const contentOfFeedback = document.getElementById("validationTextarea").value;


    const newFeedback = {"feedbackMSG":contentOfFeedback, "firstName":firstNameOfFeedback, "lastName":lastNameOfFeedback};

    // javascript object (newFeedback) -> json -> java object (BookItem.class)

    ajaxCreateFeedback(newFeedback, indicateSuccess, indicateFailure);

}


function indicateSuccess() {
    const msg = document.getElementById("display-msg");
    msg.hidden = false;
    msg.innerText = "Feedback successfully created";
}

function indicateFailure() {
    const msg = document.getElementById("display-msg");
    msg.hidden = false;
    msg.innerText = "Error: Feedback unsuccessfully created";
}
