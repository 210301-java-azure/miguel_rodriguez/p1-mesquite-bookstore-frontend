document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event) {
    // stopping the form from sending a GET request by default to the same URL (Uniform Resource Locator)
    event.preventDefault();

    const username = document.getElementById("inputEmail").value;
    const password = document.getElementById("inputPassword").value;

    ajaxLogin(username, password, successfulLogin, failedLogin);

}

function successfulLogin (xhr) {
    console.log("login successful");
    const authToken = xhr.getResponseHeader("Authorization");
    console.log("authToken: ", authToken);
    sessionStorage.setItem("token",authToken);
    if(authToken == "admin-auth-token") {
        window.location.href = "feedback.html"
    } else {
        window.location.href = "../views/home.html";  
    }

    
    //window.location.href = "../views/home.html";
}



function failedLogin(xhr) {
    const errorContainer = document.getElementById("error-msg");
    errorContainer.hidden = false;
    errorContainer.innerText = xhr.responseText;
}
