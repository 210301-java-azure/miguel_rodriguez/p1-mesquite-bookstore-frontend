const xhr = new XMLHttpRequest();   // ready state 0


/**
 * v0:
 * xhr.open("GET", "http://localhost:80/books");
 * 
 */

xhr.open("GET", "http://52.148.186.98/books");     // ready state 1

// v1: setting the Auth header with this value instead
// let token = sessionStorage.getItem("jwt");   

xhr.setRequestHeader("Authorization", "admin-auth-token");

xhr.onreadystatechange = function() {
    if(xhr.readyState == 4) {
        if(xhr.status == 200) {
            const books = JSON.parse(xhr.responseText);
            renderBookItemsInTable(books);
        } else {
            console.log("Error: Unsuccessful read...");
        }
    }
}

// initially: xhr.send();
xhr.send(new Blob());     // ready state 2, 3, 4 follow


function renderBookItemsInTable(bookItemsList) {
    document.getElementById("books-table").hidden = false;
    const tableBody = document.getElementById("books-table-body");
    for(let book of bookItemsList) {
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${book.bookID}</td><td>${book.bookTitle}</td><td>${book.condition}</td><td>${book.price}</td>`;
        tableBody.appendChild(newRow);
    }

}