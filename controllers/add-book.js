document.getElementById("addingBook-form").addEventListener("submit", addNewBookItem);

function addNewBookItem(event) {
    event.preventDefault();
    const bookTitle = document.getElementById("inputBookTitle").value;
    const bookPrice = document.getElementById("inputBookPrice").value;

    const newBook = {"bookTitle":bookTitle, "price":bookPrice};

    console.log("newBook:", newBook);

    // javascript object (newBook) -> json -> java object (BookItem.class)
    ajaxCreateBookItem(newBook, indicateSuccess, indicateFailure);

}


function indicateSuccess() {
    const msg = document.getElementById("display-msg");
    msg.hidden = false;
    msg.innerText = "New book item successfully created";
}

function indicateFailure() {
    const msg = document.getElementById("display-msg");
    msg.hidden = false;
    msg.innerText = "Error: New book item unsuccessfully created";
}
