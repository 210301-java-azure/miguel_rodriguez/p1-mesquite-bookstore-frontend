const xhr = new XMLHttpRequest();   // ready state 0

/**
 * v0:
 * xhr.open("GET", "http://localhost:80/feedback");
 * 
 */


xhr.open("GET", "http://52.148.186.98/feedback");     // ready state 1

// v1: setting the Auth header with this value instead
// let token = sessionStorage.getItem("jwt");   

xhr.setRequestHeader("Authorization", "admin-auth-token");

xhr.onreadystatechange = function() {
    if(xhr.readyState == 4) {
        if(xhr.status == 200) {
            const feedback = JSON.parse(xhr.responseText);
            renderFeedbackInTable(feedback);
        } else {
            console.log("Error: Unsuccessful read...");
        }
    }
}


// initially: xhr.send();
xhr.send(new Blob());     // ready state 2, 3, 4 follow


function renderFeedbackInTable(feedbackList) {
    document.getElementById("feedback-table").hidden = false;
    const tableBody = document.getElementById("feedback-table-body");
    for(let feedback of feedbackList) {
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${feedback.feedbackID}</td><td>${feedback.firstName}</td><td>${feedback.lastName}</td><td>${feedback.feedbackMSG}</td>`;
        tableBody.appendChild(newRow);
    }

}