const token = sessionStorage.getItem("token");

// take this token, and check it against the server to make sure it is a valid token

if(token) {
    // if there is a token, allow users to create a new book item and to log out; if not, allow them to log in
    document.getElementById("login-nav-item").hidden = true;

    // toolboox
    document.getElementById("toolbox-nav-item").hidden = false;
    
    
    document.getElementById("logout-nav-item").hidden = false;
}

// associate the logout functionality with clicking on the logout item on the nav bar
document.getElementById("logout-nav-item").addEventListener("click", logout);

function logout() {
    sessionStorage.removeItem("token");
    document.getElementById("login-nav-item").hidden = false;
    
    // toolboox
    document.getElementById("toolbox-nav-item").hidden = true;
    
    document.getElementById("logout-nav-item").hidden = true;
}