function sendAjaxRequest(method, url, body, successCallback, failureCallback, username, authToken) {
    
    const xhr = new XMLHttpRequest();   // ready state: 0
    xhr.open(method, url);  // ready state: 1

    /**
     * could set the Auth header with this value
     * let token = sessionStorage.getItem("jwt");
     *  
     */ 

    
    if(authToken) {
        xhr.setRequestHeader("Authorization", authToken);
    }

    if(username) {
        xhr.setRequestHeader("Username", username);
    }

    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) {
            if(xhr.status > 199 && xhr.status < 300) {
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if(body) {
        xhr.send(body);
    } else {
        xhr.send(); // ready state 2, 3, 4 follow
    }

}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken) {
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, undefined, authToken);
}

function sendAjaxGet(url, successCallback, failureCallback, username, authToken) {
    
    //const authToken = "admin-auth-token";
    
    sendAjaxRequest("GET", url, undefined, successCallback, failureCallback, username, authToken);
}


function ajaxLogin(username, password, successCallback, failureCallback) {
    // e.g., username=...&password=...
    const payload = `userName=${username}&password=${password}`;
    // v0:
    sessionStorage.setItem("Username", username);

    const authToken = "admin-auth-token";


    /**
     * v0:
     * 
     * sendAjaxPost("http://localhost:80/login",payload, successCallback, failureCallback, authToken);
     * http://52.148.186.98/
     */
    sendAjaxPost("http://52.148.186.98/login",payload, successCallback, failureCallback, authToken); 


}

function ajaxCreateBookItem(bookItem, successCallback, failureCallback) {
    const bookItemJSON = JSON.stringify(bookItem);

    //const authToken = sessionStorage.getItem("token");

    const authToken = "admin-auth-token";

    
    
    /** 
     * v0:
     * sendAjaxPost("http://localhost:80/books", bookItemJSON, successCallback, failureCallback, authToken);
    */
    sendAjaxPost("http://52.148.186.98/books", bookItemJSON, successCallback, failureCallback, authToken);

}

function ajaxCreateFeedback(feedback, successCallback, failureCallback) {
   
    const feedbackJSON = JSON.stringify(feedback);
    
    //const authToken = sessionStorage.getItem("token");

    const authToken = "admin-auth-token";



    /**
     * 
     * v0:
     * sendAjaxPost("http://localhost:80/feedback", feedbackJSON, successCallback, failureCallback, authToken); 
     */ 
    sendAjaxPost("http://52.148.186.98/feedback", feedbackJSON, successCallback, failureCallback, authToken);
    

}
